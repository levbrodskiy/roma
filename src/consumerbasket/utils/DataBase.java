package consumerbasket.utils;

import consumerbasket.entities.DBRecord;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase implements MySQLConfiguration{
    private static Connection connection;

    static {
        try {
            connection = getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws Exception{
        Connection connection = null;
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        return connection;
    }

    public void insert(double bakeryPrice, double pastaAndCerealsPrice, double potatoPrice,
                       double vegetablesPrice, double fruitPrice, double sugarPrice, double meatPrice,
                       double fishPrice, double milkPrice, double eggsPrice, double butterPrice,
                       double teaPrice, String name) throws SQLException {

        String sql = "INSERT INTO consumer_norm " +
                "(bakeryPrice, pastaAndCerealsPrice, potatoPrice, vegetablesPrice, fruitPrice," +
                "sugarPrice, meatPrice, fishPrice, milkPrice, eggsPrice, butterPrice, teaPrice, name)" +
                " Values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setDouble(1, bakeryPrice);
        preparedStatement.setDouble(2, pastaAndCerealsPrice);
        preparedStatement.setDouble(3, potatoPrice);
        preparedStatement.setDouble(4, vegetablesPrice);
        preparedStatement.setDouble(5, fruitPrice);
        preparedStatement.setDouble(6, sugarPrice);
        preparedStatement.setDouble(7, meatPrice);
        preparedStatement.setDouble(8, fishPrice);
        preparedStatement.setDouble(9, milkPrice);
        preparedStatement.setDouble(10, eggsPrice);
        preparedStatement.setDouble(11, butterPrice);
        preparedStatement.setDouble(12, teaPrice);
        preparedStatement.setString(13, name);

        preparedStatement.executeUpdate();
    }

    public List<DBRecord> select() throws SQLException {
        List<DBRecord> result = new ArrayList<>();
        String query = "select * from consumer_norm;";
        ResultSet resultSet = connection.createStatement().executeQuery(query);

        while(resultSet.next()){

        DBRecord dbRecord = new DBRecord();
        dbRecord.setBakeryPrice(resultSet.getDouble("bakeryPrice"));
        dbRecord.setPastaAndCerealsPrice(resultSet.getDouble("pastaAndCerealsPrice"));
        dbRecord.setPotatoPrice(resultSet.getDouble("potatoPrice"));
        dbRecord.setVegetablesPrice(resultSet.getDouble("vegetablesPrice"));
        dbRecord.setFruitPrice(resultSet.getDouble("fruitPrice"));
        dbRecord.setSugarPrice(resultSet.getDouble("sugarPrice"));
        dbRecord.setMeatPrice(resultSet.getDouble("meatPrice"));
        dbRecord.setFishPrice(resultSet.getDouble("fishPrice"));
        dbRecord.setMilkPrice(resultSet.getDouble("milkPrice"));
        dbRecord.setEggsPrice(resultSet.getDouble("eggsPrice"));
        dbRecord.setButterPrice(resultSet.getDouble("butterPrice"));
        dbRecord.setTeaPrice(resultSet.getDouble("teaPrice"));
        dbRecord.setName(resultSet.getString("name"));

        result.add(dbRecord);
        }
        return result;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }
}
