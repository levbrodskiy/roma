package consumerbasket.popupwindows;

import consumerbasket.entities.Person;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class CalcWindow {
    public void open() throws IOException {
        Stage primaryStage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/consumerbasket/views/calcWindow.fxml"));
        primaryStage.setTitle("Заполнение");
        primaryStage.setScene(new Scene(root, 550, 500));
        primaryStage.show();
    }
}
