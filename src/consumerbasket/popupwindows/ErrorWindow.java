package consumerbasket.popupwindows;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ErrorWindow {
    public static void openWindow(String message) {
        Stage stage = new Stage();

        Label numberAllSymbols = new Label(message);
        numberAllSymbols.setLayoutX(55);
        numberAllSymbols.setLayoutY(20);

        Group group = new Group(numberAllSymbols);
        Scene scene = new Scene(group);

        stage.setScene(scene);
        stage.setWidth(250);
        stage.setHeight(100);

        stage.setTitle("Ошибка");
        stage.show();
    }
}
