package consumerbasket.popupwindows;

import consumerbasket.entities.Group;
import consumerbasket.utils.InfoTableCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class InfoWindow {
    public static void open(){
        ObservableList<InfoTableCell> data = FXCollections.observableArrayList();
        data.add(new InfoTableCell("Хлебобулочные",
                Group.ADULT_PEOPLE.getBakery(),
                Group.CHILDREN.getBakery(),
                Group.RETIREES.getBakery()));

        data.add(new InfoTableCell("Крупы",
                Group.ADULT_PEOPLE.getPastaAndCereals(),
                Group.CHILDREN.getPastaAndCereals(),
                Group.RETIREES.getPastaAndCereals()));

        data.add(new InfoTableCell("Картофель",
                Group.ADULT_PEOPLE.getPotato(),
                Group.CHILDREN.getPotato(),
                Group.RETIREES.getPotato()));

        data.add(new InfoTableCell("Овощи",
                Group.ADULT_PEOPLE.getVegetables(),
                Group.CHILDREN.getVegetables(),
                Group.RETIREES.getVegetables()));

        data.add(new InfoTableCell("Фрукты",
                Group.ADULT_PEOPLE.getFruit(),
                Group.CHILDREN.getFruit(),
                Group.RETIREES.getFruit()));

        data.add(new InfoTableCell("Сахар",
                Group.ADULT_PEOPLE.getSugar(),
                Group.CHILDREN.getSugar(),
                Group.RETIREES.getSugar()));

        data.add(new InfoTableCell("Мясо",
                Group.ADULT_PEOPLE.getMeat(),
                Group.CHILDREN.getMeat(),
                Group.RETIREES.getMeat()));

        data.add(new InfoTableCell("Рыба",
                Group.ADULT_PEOPLE.getFish(),
                Group.CHILDREN.getFish(),
                Group.RETIREES.getFish()));

        data.add(new InfoTableCell("Молочные продукты",
                Group.ADULT_PEOPLE.getMilk(),
                Group.CHILDREN.getMilk(),
                Group.RETIREES.getMilk()));

        data.add(new InfoTableCell("Яйца",
                Group.ADULT_PEOPLE.getEggs(),
                Group.CHILDREN.getEggs(),
                Group.RETIREES.getEggs()));

        data.add(new InfoTableCell("Масла и жиры",
                Group.ADULT_PEOPLE.getButter(),
                Group.CHILDREN.getButter(),
                Group.RETIREES.getButter()));

        data.add(new InfoTableCell("Чай",
                Group.ADULT_PEOPLE.getTea(),
                Group.CHILDREN.getTea(),
                Group.RETIREES.getTea()));

        TableView<InfoTableCell> table = new TableView<InfoTableCell>(data);

        TableColumn<InfoTableCell, String> nameColumn
                = new TableColumn<InfoTableCell, String>("Название");

        TableColumn<InfoTableCell, Double> adultColumn
                = new TableColumn<InfoTableCell, Double>("Взрослые");

        TableColumn<InfoTableCell, Double> childrenColumn
                = new TableColumn<InfoTableCell, Double>("Дети");

        TableColumn<InfoTableCell, Double> retireesColumn
                = new TableColumn<InfoTableCell, Double>("Пенсионеры");


        table.getColumns().addAll(nameColumn, adultColumn, childrenColumn, retireesColumn);

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        adultColumn.setCellValueFactory(new PropertyValueFactory<>("adultPeopleValue"));
        childrenColumn.setCellValueFactory(new PropertyValueFactory<>("childrenValue"));
        retireesColumn.setCellValueFactory(new PropertyValueFactory<>("retirees"));

        table.setItems(data);


        StackPane root = new StackPane();
        root.setPadding(new Insets(10));
        root.getChildren().add(table);

        Stage stage = new Stage();
        stage.setTitle("Меню");

        Scene scene = new Scene(root, 380, 300);
        stage.setScene(scene);
        stage.show();
    }
}
